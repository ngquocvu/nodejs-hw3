# NodeJS HW3

## .env file format
PORT=
SECRET_KEY=
ACCESS_TOKEN_LIFETIME=
DATABASE_URL=

## How to run app
1. config .env file
2. npm install
3. npm start
