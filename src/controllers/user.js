import User from '../models/user.js';
import mongoose from 'mongoose';
import {loginSchema, registerSchema} from '../utils/validationSchema.js';

export const register = async (req, res) => {
  try {
    await registerSchema.validateAsync(req.body);
    if (!req.body.email || !req.body.password || !req.body.role) {
      return res.status(400).json({
        message: 'email or password or role is empty',
      });
    }
    const user = new User({
      _id: new mongoose.Types.ObjectId(),
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
      createdDate: Date.now(),
    });

    // await to see if the save() function return an error
    await user.save();
    return await res
        .status(201)
        .json({message: 'Profile created successfully'});
  } catch (error) {
    if (error && error.code === 11000) {
      return res.status(500).json({message: 'Duplicated email'});
    }
    return res.status(500).json({message: error.message});
  }
};

export const login = async (req, res) => {
  try {
    await loginSchema.validateAsync(req.body);
    if (!req.body.email || !req.body.password) {
      return res.status(400).json({
        message: 'email or password is empty',
      });
    }
    const user = await User.findUserByPasswordAndEmail(
        req.body.email,
        req.body.password,
    );
    const jwt = await user.generateAccessToken();
    return res.status(200).json({jwt_token: jwt});
  } catch (error) {
    return res.status(500).json({message: error.message});
  }
};
