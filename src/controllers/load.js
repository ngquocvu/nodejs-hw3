import Load from '../models/load.js';
import mongoose from 'mongoose';
import {LOAD_STATE, LOAD_STATUS, ROLE} from '../utils/constants.js';
import {findTruck} from '../middleware/truck.js';
import {updateLoadSchema} from '../utils/validationSchema.js';

export const addALoad = async (req, res) => {
  try {
    const load = new Load({
      _id: new mongoose.Types.ObjectId(),
      created_by: req.user._id,
      status: Object.values(LOAD_STATUS)[0],
      assigned_to: null,
      state: LOAD_STATE[0],
      name: req.body.name,
      payload: req.body.payload,
      pickup_address: req.body.pickup_address,
      delivery_address: req.body.delivery_address,
      dimensions: {
        width: req.body.dimensions.width,
        length: req.body.dimensions.length,
        height: req.body.dimensions.height,
      },
      created_date: Date.now(),
    });
    await load.save();
    res.status(200), res.json({message: 'Load created successfully'});
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

export const getUserLoad = async (req, res) => {
  try {
    let loads=[];
    if (req.user.role===ROLE.DRIVER) {
      loads = await Load.find({
        assignedTo: req.user._id,
        status: 'ASSIGNED',
      },
      {},
      {limit: req.query.limit || 0,
        skip: req.query.offset || 0});
    } else if (req.user.role===ROLE.SHIPPER) {
      loads = await Load.find({
        created_by: req.user._id,
      },
      {},
      {limit: req.query.limit || 0,
        skip: req.query.offset || 0});
    }
    if (loads.length===0) {
      return res.status(200).json({
        message: 'You do not have any loads yet.',
        loads: [],
      });
    }
    return res.status(200).json({loads});
  } catch (e) {
    res.status(500).json({message: e.message});
  }
};

export const postALoad = async (req, res) => {
  try {
    const loadId = req.params.id;
    const shipperId = req.user._id;

    const previousLoad = await Load.findById(loadId);

    if (!previousLoad) {
      return res.status(400).json({status: 'This load does not exist'});
    }

    if (!previousLoad.created_by.equals(shipperId)) {
      return res.status(403).json({status: 'Access denied'});
    }

    if (previousLoad.status !== 'NEW') {
      return res.status(400).json({
        status: 'Cannot post the load. Its status is not new.',
      });
    }

    await Load.findByIdAndUpdate(loadId, {status: 'POSTED'});
    const truckState = findTruck(loadId);

    truckState.then((truck) => {
      if (truck.state === 'No truck') {
        return res.status(200).json({
          message: 'No drivers found',
        });
      } else if (truck.state === 'En route') {
        return res.status(200).json({
          message: 'Load posted successfully',
          assigned_to: truck.driverId,
        });
      } else {
        return res.status(500).json({
          message: truck.state,
        });
      }
    });
  } catch (e) {
    res.status(500).json({
      message: 'Something went wrong',
    });
  }
};

export const iterateLoad = async (req, res)=> {
  try {
    const loadId = req.params.id;
    const driverId = req.user._id;
    const load = await Load.findById(loadId);

    if (!load) {
      return res.status(400).json({message: 'This load does not exist'});
    }

    if (load.assigned_to != driverId) {
      return res.status(403).json({message: 'Access denied'});
    }

    if (load.status !== 'ASSIGNED') {
      return res.status(400).json({
        message: 'Forbidden to change state',
      });
    }

    let newState;

    switch (load.state) {
      case 'Ready to Pick Up':
        newState = 'En route to Pick Up';
        break;
      case 'En route to Pick Up':
        newState = 'Arrived to Pick Up';
        break;
      case 'Arrived to Pick Up':
        newState = 'En route to Delivery';
        break;
      case 'En route to Delivery':
        newState = 'Arrived to Delivery';
        break;
      case 'Arrived to Delivery':
        newState = null;
        break;
      default:
        newState = null;
        break;
    }

    if (!newState) {
      return res.status(200).json({
        message: 'Load is already arrived to delivery',
      });
    }

    const update = {
      state: newState,
      $push: {
        logs: {
          message:
          `Current state of load is ${newState}`,
          time: Date.now(),
        },
      },
    };

    if (newState === 'Arrived to Delivery') {
      update['status'] = 'SHIPPED';

      await Truck.findOneAndUpdate(
          {assigned_to: driverId, status: 'OL'},
          {status: 'IS'},
      );
    }

    await Load.findByIdAndUpdate(
        loadId,
        update,
    );
    res.status(200).json({
      message: 'Load status changed successfully',
    });
  } catch (e) {
    res.status(500).json({
      message: 'Something went wrong',
    });
  }
};

export const getUserLoadById = async (req, res)=> {
  if (!req.params.id) {
    return res.status(400).json({message: req.params.id});
  }
  try {
    const load = await Load.findOne({
      created_by: req.user._id,
      _id: req.params.id,
    });
    if (!load) {
      res.status(400).json({message: 'Load has not been found'});
    }
    return res.status(200).json(load);
  } catch (e) {
    res.status(500).json({message: e.message});
  }
};

export const updateUserLoadById = async (req, res)=> {
  if (!req.params.id) {
    return res.status(400).json({message: req.params.id});
  }
  try {
    await updateLoadSchema.validateAsync(req.body);
    await Load.findOneAndUpdate({
      created_by: req.user._id,
      _id: req.params.id,
      status: 'NEW',
    }, {name: req.body.name, payload: req.body.payload,
      pickup_address: req.body.pickup_address,
      delivery_address: req.body.delivery_address,
      dimensions: {
        width: req.body.width,
        length: req.body.length,
        height: req.body.height,
      },
    });
    return res.status(200).json('Load details changed successfully');
  } catch (e) {
    res.status(500).json({message: e.message});
  }
};


export const deleteUserLoadById = async (req, res)=> {
  if (!req.params.id) {
    return res.status(400).json({message: req.params.id});
  }
  try {
    await Load.findOneAndDelete({
      created_by: req.user._id,
      _id: req.params.id,
      status: 'NEW',
    });
    return res.status(200).json('Load details deleted successfully');
  } catch (e) {
    res.status(500).json({message: e.message});
  }
};


export const getShippingInfoByLoadId = async (req, res)=> {
  if (!req.params.id) {
    return res.status(400).json({message: 'Load\'s Id is empty'});
  }
  try {
    const load = await Load.findOne({
      created_by: req.user._id,
      _id: req.params.id,
    });
    if (!load) {
      res.status(400).json({message: 'Load has not been found'});
    }
    return res.status(200).json(load);
  } catch (e) {
    res.status(500).json({message: e.message});
  }
};
