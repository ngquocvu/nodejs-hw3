export const ROLE = {SHIPPER: 'SHIPPER', DRIVER: 'DRIVER'};
export const TYPE = {SPRINTER: 'SPRINTER',
  SMALL_STRAIGHT: 'SMALL STRAIGHT', LARGE_STRAIGHT: 'LARGE STRAIGHT'};
export const STATUS = {IS: 'IS', OL: 'OL'};
export const LOAD_STATUS =
{
  NEW: 'NEW',
  POSTED: 'POSTED',
  ASSIGNED: 'ASSIGNED',
  SHIPPED: 'SHIPPED',
};
export const LOAD_STATE = [
  'Ready to Pick Up',
  'En route to Pick up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];
