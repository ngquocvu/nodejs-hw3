import Joi from 'joi';
import {ROLE, TYPE} from './constants.js';


export const loginSchema = Joi.object({
  email: Joi.string().email().min(2).required(),
  password: Joi.string().min(8).required(),
});

export const registerSchema = Joi.object({
  email: Joi.string().email().min(2).required(),
  password: Joi.string().min(8).required(),
  role: Joi.string().valid(...Object.values(ROLE)),
});

export const truckTypeSchema = Joi.object({
  type: Joi.string().valid(...Object.values(TYPE)),
});

export const updateLoadSchema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: {
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  },
});

