import express from 'express';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import logger from 'morgan';
import cors from 'cors';
import MainRouter from './routes/main.js';

const app = express();
dotenv.config();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(logger('dev'));

mongoose
    .connect(process.env.DATABASE_URL)
    .then(() => console.log('connect'))
    .catch((e) => console.log(e));

app.use('/api', MainRouter);

app.use('/', (req, res) => {
  res.status(200).json({message: 'Welcome'});
});

const port = process.env.PORT || 8080;
app.listen(port, () => console.log('Started on port' + port));
