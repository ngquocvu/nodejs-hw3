import Load from '../models/load.js';
import Truck from '../models/truck.js';
import {LOAD_STATE, LOAD_STATUS} from '../utils/constants.js';
export const findTruck = async (loadId) => {
  try {
    const load = await Load.findById(loadId);
    const truck = await Truck.where('assigned_to').ne(null)
        .where('status').equals('IS')
        .findOne();
    const updateLogs = [...load.logs];

    if (!truck) {
      updateLogs.push({
        message:
            'Changed status to NEW. There is no appropriate truck',
        time: Date.now(),
      });

      await Load.findByIdAndUpdate(
          loadId,
          {
            status: 'NEW',
            logs: updateLogs,
          },
      );
      return {
        state: 'No truck',
      };
    }

    await Truck.findByIdAndUpdate(truck._id, {status: 'OL'});

    updateLogs.push({
      message:
          'Load is assigned. Driver is en route',
      time: Date.now(),
    });

    await Load.findByIdAndUpdate(
        loadId,
        {
          status: LOAD_STATUS.ASSIGNED,
          state: LOAD_STATE[1],
          logs: updateLogs,
          assigned_to: truck.assigned_to,
        },
    );
    return {
      state: 'En route',
      driverId: truck.assignedTo,
    };
  } catch (e) {
    return {
      state: 'Error',
    };
  }
};

