import User from '../models/user.js';
import jwt from 'jsonwebtoken';


export const auth = async (req, res, next) => {
  const token =
    req.header('Authorization') && req.header('Authorization').split(' ')[1];
  if (!token) {
    return res
        .status(403)
        .json({message: 'A token is required for authentication'});
  }
  try {
    const decoded = jwt.decode(token, process.env.SECRET_KEY);
    if (!decoded) {
      return res.status(400).json({message: 'Token is not valid'});
    }
    const user = await User.findOne({email: decoded.email});
    if (!user) {
      return res.status(400).json({message: 'User is not in database'});
    }
    req.user = user;
    next();
  } catch (error) {
    res.status(500).json({message: error.message});
  }
};

export const hasRole = (req, res, next, role) => {
  if (req.user.role === role) {
    next();
  } else {
    return res.status(401).json({message: 'Forbidden'});
  }
};

