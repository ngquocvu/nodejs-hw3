import express from 'express';
import mongoose from 'mongoose';
import {ROLE, STATUS} from '../utils/constants.js';
import {hasRole, auth} from '../middleware/auth.js';
import Truck from '../models/truck.js';
import {truckTypeSchema} from '../utils/validationSchema.js';

const TruckRoute = new express.Router();

TruckRoute.use(auth);
TruckRoute.use((req, res, next)=>hasRole(req, res, next, ROLE.DRIVER));

TruckRoute.get('/',
    async (req, res)=> {
      try {
        const trucks = await Truck.find({created_by: req.user._id});
        return res.status(200).json({trucks});
      } catch (error) {
        return res.status(500).json({message: error.message});
      }
    });

TruckRoute.post('/', async (req, res)=> {
  try {
    await truckTypeSchema.validateAsync(req.body);
    const truck = new Truck({
      _id: new mongoose.Types.ObjectId(),
      created_by: req.user._id,
      assigned_to: null,
      type: req.body.type,
      status: STATUS.IS,
      created_date: Date.now(),
    });
    await truck.save();
    return res.status(200).json({message: 'Truck created successfully'});
  } catch (error) {
    return res.status(500).json({message: error.message});
  }
});

TruckRoute.get('/:id',
    async (req, res)=> {
      if (!req.params.id) {
        return res.status(400).json(`Truck's id is missing`);
      }
      try {
        const truck = await Truck.findOne({_id: req.params.id,
          created_by: req.user._id});
        if (!truck) {
          return res.status(400).json({message: 'Could not find truck'});
        }
        return res.status(200).json({truck});
      } catch (error) {
        return res.status(500).json({message: error.message});
      }
    });

TruckRoute.put('/:id',
    async (req, res)=> {
      if (!req.params.id) {
        return res.status(400).json(`Truck's id is missing`);
      }
      if (!req.body.type) {
        return res.status(400).json('type is missing');
      }
      try {
        const truck = await Truck.findOne({_id: req.params.id,
          created_by: req.user._id});
        truck.type = req.body.type;
        await truck.save();
        return res.status(200)
            .json({message: 'Truck details changed successfully'});
      } catch (error) {
        return res.status(500).json({message: error.message});
      }
    });

TruckRoute.delete('/:id',
    async (req, res)=> {
      if (!req.params.id) {
        return res.status(400).json(`Truck's id is missing`);
      }
      try {
        const truck = await Truck.findOne({_id: req.params.id,
          created_by: req.user._id});
        if (!truck) {
          return res.status(400).json({message: 'Could not find truck'});
        }
        await truck.delete();
        return res.status(200).json({message: 'Truck deleted successfully'});
      } catch (error) {
        return res.status(500).json({message: error.message});
      }
    });

TruckRoute.post('/:id/assign',
    async (req, res)=> {
      if (!req.params.id) {
        return res.status(400).json(`Truck's id is missing`);
      }
      try {
        const truck = await Truck.findOne({_id: req.params.id,
          created_by: req.user._id});
        if (!truck) {
          return res.status(400).json({message: 'Could not find truck'});
        }
        truck.assigned_to = req.user._id;
        await truck.save();
        return res.status(200).json({message: 'Truck assigned successfully'});
      } catch (error) {
        return res.status(500).json({message: error.message});
      }
    });


// Should driver see, update, delete others trucks.

export default TruckRoute;
