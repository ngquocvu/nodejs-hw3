import express from 'express';
import {login, register} from '../controllers/user.js';

const AuthRouter = new express.Router();

AuthRouter.use('/register', register);
AuthRouter.use('/login', login);

export default AuthRouter;
