import express from 'express';
import AuthRouter from './auth.js';
import UserRouter from './users.js';
import TruckRouter from './truck.js';
import LoadRouter from './load.js';

const MainRouter = new express.Router();
MainRouter.use('/auth', AuthRouter);
MainRouter.use('/users', UserRouter);
MainRouter.use('/trucks', TruckRouter);
MainRouter.use('/loads', LoadRouter);

export default MainRouter;
