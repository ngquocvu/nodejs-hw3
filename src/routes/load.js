import express from 'express';
import {addALoad, deleteUserLoadById,
  getShippingInfoByLoadId, getUserLoad, getUserLoadById,
  iterateLoad, postALoad, updateUserLoadById}
  from '../controllers/load.js';
import {auth, hasRole} from '../middleware/auth.js';
import {ROLE} from '../utils/constants.js';

const LoadRouter = new express.Router();

LoadRouter.use(auth);

LoadRouter.post('/', (req, res, next)=>
  hasRole(req, res, next, ROLE.SHIPPER), addALoad);

LoadRouter.get('/', getUserLoad);

LoadRouter.post('/:id/post',
    (req, res, next)=> hasRole(req, res, next, ROLE.SHIPPER),
    postALoad);

LoadRouter.post('/:id/state',
    (req, res, next)=> hasRole(req, res, next, ROLE.DRIVER),
    iterateLoad);

LoadRouter.get('/:id',
    (req, res, next)=>hasRole(req, res, next, ROLE.SHIPPER),
    getUserLoadById);

LoadRouter.put('/:id',
    (req, res, next)=>hasRole(req, res, next, ROLE.SHIPPER),
    updateUserLoadById);

LoadRouter.delete('/:id',
    (req, res, next)=>hasRole(req, res, next, ROLE.SHIPPER),
    deleteUserLoadById);

LoadRouter.get('/:id/shipping_info',
    (req, res, next)=>hasRole(req, res, next, ROLE.SHIPPER),
    getShippingInfoByLoadId,
);

export default LoadRouter;
