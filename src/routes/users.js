import express from 'express';
import {auth, hasRole} from '../middleware/auth.js';
import user from '../models/user.js';
import bcrypt from 'bcryptjs';
import {ROLE} from '../utils/constants.js';

const UserRouter = new express.Router();

UserRouter.get('/me', auth, (req, res) => {
  res.status(200).json({
    users: {
      _id: req.user._id,
      role: req.user.role,
      email: req.user.email,
      created_date: req.user.createdDate,
    },
  });
});

UserRouter.delete('/me', auth, (req, res, next)=>
  hasRole(req, res, next, ROLE.SHIPPER), async (req, res) => {
  try {
    await user.deleteOne({username: req.user.username});
    res.status(200).json({
      message: 'Success',
    });
  } catch (e) {
    res.status(500).json({message: e});
  }
});

UserRouter.patch('/me/password', auth, async (req, res) => {
  try {
    if (!req.body.newPassword || !req.body.oldPassword || !req.user.password) {
      return res.status(400).json({message: 'Invalid input'});
    }
    const isMatched = await bcrypt.compare(
        req.body.oldPassword,
        req.user.password,
    );

    if (isMatched) {
      req.user.password = req.body.newPassword;
      await req.user.save();
      res.status(200).json({message: 'success'});
    } else {
      res.status(400).json({message: 'Password is not matched'});
    }
  } catch (e) {
    console.log(e);
    res.status(500).json({message: e.message});
  }
});

export default UserRouter;
