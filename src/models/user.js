/* eslint-disable no-invalid-this */
import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import {ROLE} from '../utils/constants.js';

const userSchema = new mongoose.Schema(
    {
      _id: mongoose.Schema.Types.ObjectId,
      email: {
        type: String,
        required: true,
        unique: true,
      },
      password: {
        type: String,
        required: true,
        minlength: 8,
      },
      createdDate: {
        type: Date,
        required: true,
      },
      role: {
        type: String,
        enum: Object.values(ROLE),
      },
    },
    {collection: 'users'},
);

userSchema.methods.generateAccessToken = async function() {
  const user = this;
  return jwt.sign(
      {_id: user._id, email: user.email},
      process.env.SECRET_KEY,
      {expiresIn: process.env.ACCESS_TOKEN_LIFETIME},
  );
};

userSchema.statics.findUserByPasswordAndEmail = async function(
    email,
    password,
) {
  const user = await this.findOne({email: email});
  if (!user) {
    throw new Error('Email or password not match');
  }
  const isMatched = await bcrypt.compare(password, user.password);
  if (!isMatched) {
    throw new Error('Email or password not match');
  }
  return user;
};

userSchema.pre('save', async function(next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

export default mongoose.model('User', userSchema);
