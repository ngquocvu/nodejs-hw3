import mongoose from 'mongoose';
import {TYPE, STATUS} from '../utils/constants.js';

const TruckSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: false,
  },
  type: {
    type: String,
    required: true,
    enum: Object.values(TYPE),
  },
  status: {
    type: String,
    required: true,
    enum: Object.values(STATUS),
  },
  created_date: {
    type: Date,
    required: true,
  },
},
);

export default mongoose.model('truck', TruckSchema);
